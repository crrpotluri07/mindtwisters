#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 20:42:26 2019

@author: chaitanyapotluri
"""

#The change-making problem addresses the question of finding the minimum number of coins (of certain denominations) 
#that add up to a given amount of money.

# greedy method
def num_coins(cents):
    coins = [25, 10, 5, 1]
    count = 0
    for coin in coins:
        while cents >= coin:
            cents = cents - coin
            count = count + 1

    return count

print(num_coins(55))
